﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigInt : IComparable<BigInt> {

    private List<int> periods;
    private bool negative;

    public BigInt()
    {
        periods = new List<int>();
        periods.Add(0);
        negative = false;
    }

    public BigInt(BigInt bi) : this()
    {
        for (int i = 0; i < bi.PeriodCount; i++)
        {
            AddToPeriod(i, bi.GetPeriod(i));
        }
        negative = bi.negative;
    }

    public BigInt(int init) : this()
    {
        if (init < 0)
        {
            this.AddToPeriod(0, -init);
            negative = true;
        }
        else
        {
            this.AddToPeriod(0, init);
            negative = false;
        }
    }

    public override string ToString()
    {
        string result = "";
        for (int i = 0; i < PeriodCount; i++)
        {
            int period = periods[i];
            string format = "0";

            if (period >= 10 || i < PeriodCount - 1)
                format += "0";
            if (period >= 100 || i < PeriodCount - 1)
                format += "0";

            string part = period.ToString(format);
            result = i != 0 ? part + "," + result : part;
        }
        if (negative)
            result = "-" + result;
        return result;
    }

    public void setNegative(bool n)
    {
        negative = n;
    }

    int PeriodCount
    {
        get
        {
            return periods.Count;
        }
    }

    int GetPeriod(int index)
    {
        int result = 0;
        if (index < periods.Count)
            result = periods[index];
        return result;
    }

    void AddToPeriod(int index, int value)
    {
        if (value < 0)
        {
            SubtractFromPeriod(index, -value);
            return;
        }
        while (index >= periods.Count)
            periods.Add(0);
        periods[index] += value;
        if(periods[index] > 1000)
        {
            int thousands = Mathf.FloorToInt((float)periods[index] / 1000f);
            this.AddToPeriod(index + 1, thousands);
            periods[index] -= thousands * 1000;
        }
    }

    void SubtractFromPeriod(int index, int value)
    {
        periods[index] -= value;
        if(periods[index] < 0)
        {
            int thousands = Mathf.FloorToInt((float)periods[index] / -1000f) + 1;
            SubtractFromPeriod(index + 1, thousands);
            periods[index] += thousands * 1000;
        }
    }

    void RemoveExcessZeros()
    {
        int i = PeriodCount - 1;
        while (periods[i] == 0 && i > 0)
        {
            periods.RemoveAt(i);
            i--;
        }
    }

    static int CompareAbsolute(BigInt a, BigInt b)
    {
        if (a.PeriodCount > b.PeriodCount) return 1;
        else if (a.PeriodCount < b.PeriodCount) return -1;
        else
        {
            int i = a.PeriodCount - 1;
            int diff = 0;
            while (diff == 0 && i >= 0)
            {
                diff = a.GetPeriod(i) - b.GetPeriod(i);
                if (diff != 0)
                    diff = diff / Math.Abs(diff);
                i--;
            }
            return diff;
        }
    }

    static BigInt AddAbsolute(BigInt a, BigInt b)
    {
        BigInt result = new BigInt();
        for (int i = 0; i < (a.PeriodCount > b.PeriodCount ? a.PeriodCount : b.PeriodCount); i++)
        {
            result.AddToPeriod(i, a.GetPeriod(i) + b.GetPeriod(i));
        }
        return result;
    }

    static BigInt SubtractAbsolute(BigInt a, BigInt b)
    {
        if (CompareAbsolute(a, b) < 0) return SubtractAbsolute(b, a);

        BigInt result = new BigInt(a);
        for (int i = 0; i < (a.PeriodCount > b.PeriodCount ? a.PeriodCount : b.PeriodCount); i++)
        {
            result.SubtractFromPeriod(i, b.GetPeriod(i));
        }
        return result;
    }

    public int CompareTo(BigInt other)
    {
        return
            this.negative ?
            (other.negative ? -CompareAbsolute(this, other) : -1) :
            (other.negative ? 1 : CompareAbsolute(this, other));
    }

    public static bool operator <(BigInt a, BigInt b)
    {
        return a.CompareTo(b) < 0;
    }

    public static bool operator >(BigInt a, BigInt b)
    {
        return a.CompareTo(b) > 0;
    }

    public static BigInt operator +(BigInt a, BigInt b)
    {
        BigInt absolute;
        bool negative;
        if (a.negative)
        {
            if (b.negative)
            {
                absolute = AddAbsolute(a, b);
                negative = true;
            }
            else
            {
                absolute = SubtractAbsolute(a, b);
                negative = (CompareAbsolute(a, b) > 0);
            }
        }
        else
        {
            if(b.negative)
            {
                absolute = SubtractAbsolute(a, b);
                negative = (CompareAbsolute(a, b) < 0);
            }
            else
            {
                absolute = AddAbsolute(a, b);
                negative = false;
            }
        }
        absolute.negative = negative;
        absolute.RemoveExcessZeros();
        return absolute;
    }

    public static BigInt operator -(BigInt a, BigInt b)
    {
        BigInt absolute;
        bool negative;
        if (a.negative)
        {
            if (b.negative)
            {
                absolute = SubtractAbsolute(a, b);
                negative = (CompareAbsolute(a, b) > 0);
            }
            else
            {
                absolute = AddAbsolute(a, b);
                negative = true;
            }
        }
        else
        {
            if (b.negative)
            {
                absolute = AddAbsolute(a, b);
                negative = false;
            }
            else
            {
                absolute = SubtractAbsolute(a, b);
                negative = (CompareAbsolute(a, b) < 0);
            }
        }
        absolute.negative = negative;
        absolute.RemoveExcessZeros();
        return absolute;
    }
}
