﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreKeeper : MonoBehaviour {

	// Use this for initialization
	void Start () {
        BigInt a = new BigInt(int.MaxValue);
        BigInt b = new BigInt(int.MaxValue / 2);


        BigInt result = a + b;
        Debug.Log(a.ToString() + " + " + b.ToString() + " = " + result.ToString());
        result = a - b;
        Debug.Log(a.ToString() + " - " + b.ToString() + " = " + result.ToString());

        a.setNegative(true);
        result = a + b;
        Debug.Log(a.ToString() + " + " + b.ToString() + " = " + result.ToString());
        result = a - b;
        Debug.Log(a.ToString() + " - " + b.ToString() + " = " + result.ToString());

        a.setNegative(false);
        b.setNegative(true);
        result = a + b;
        Debug.Log(a.ToString() + " + " + b.ToString() + " = " + result.ToString());
        result = a - b;
        Debug.Log(a.ToString() + " - " + b.ToString() + " = " + result.ToString());

        a.setNegative(true);
        result = a + b;
        Debug.Log(a.ToString() + " + " + b.ToString() + " = " + result.ToString());
        result = a - b;
        Debug.Log(a.ToString() + " - " + b.ToString() + " = " + result.ToString());

        a = new BigInt(int.MaxValue / 2);
        b = new BigInt(int.MaxValue);

        result = a + b;
        Debug.Log(a.ToString() + " + " + b.ToString() + " = " + result.ToString());
        result = a - b;
        Debug.Log(a.ToString() + " - " + b.ToString() + " = " + result.ToString());

        a.setNegative(true);
        result = a + b;
        Debug.Log(a.ToString() + " + " + b.ToString() + " = " + result.ToString());
        result = a - b;
        Debug.Log(a.ToString() + " - " + b.ToString() + " = " + result.ToString());

        a.setNegative(false);
        b.setNegative(true);
        result = a + b;
        Debug.Log(a.ToString() + " + " + b.ToString() + " = " + result.ToString());
        result = a - b;
        Debug.Log(a.ToString() + " - " + b.ToString() + " = " + result.ToString());

        a.setNegative(true);
        result = a + b;
        Debug.Log(a.ToString() + " + " + b.ToString() + " = " + result.ToString());
        result = a - b;
        Debug.Log(a.ToString() + " - " + b.ToString() + " = " + result.ToString());

        a = new BigInt(int.MaxValue);
        b = new BigInt(int.MaxValue);

        result = a + b;
        Debug.Log(a.ToString() + " + " + b.ToString() + " = " + result.ToString());
        result = a - b;
        Debug.Log(a.ToString() + " - " + b.ToString() + " = " + result.ToString());

        a.setNegative(true);
        result = a + b;
        Debug.Log(a.ToString() + " + " + b.ToString() + " = " + result.ToString());
        result = a - b;
        Debug.Log(a.ToString() + " - " + b.ToString() + " = " + result.ToString());

        a.setNegative(false);
        b.setNegative(true);
        result = a + b;
        Debug.Log(a.ToString() + " + " + b.ToString() + " = " + result.ToString());
        result = a - b;
        Debug.Log(a.ToString() + " - " + b.ToString() + " = " + result.ToString());

        a.setNegative(true);
        result = a + b;
        Debug.Log(a.ToString() + " + " + b.ToString() + " = " + result.ToString());
        result = a - b;
        Debug.Log(a.ToString() + " - " + b.ToString() + " = " + result.ToString());
    }
	
	// Update is called once per frame
	void Update () {

	}

    private void OnApplicationQuit()
    {
        
    }
}
